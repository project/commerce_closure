<?php

function commerce_closure_settings() {
  $form = array();

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
  );

  $form['settings']['commerce_closure_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable closure'),
    '#default_value' => variable_get('commerce_closure_enable', FALSE),
    '#description' => t('If this option is not checked, closure will always be disabled, even if period is set.')
  );

  $form['settings']['commerce_closure_start'] = array(
    '#type' => 'date',
    '#title' => t('Closing date start'),
    '#default_value' => variable_get('commerce_closure_start'),
  );

  $form['settings']['commerce_closure_end'] = array(
    '#type' => 'date',
    '#title' => t('Closing date end'),
    '#default_value' => variable_get('commerce_closure_end'),
  );

  $form['settings']['commerce_closure_options'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Define what to do when closure is enabled'),
    '#options' => array(
      'hide_price' => t('Hide price'),
      'hide_cart' => t('Hide cart'),
      'display_message' => t('Display informative message'),
    ),
    '#default_value' => variable_get('commerce_closure_options', array()),
    '#description' => t('If this option is not checked, closure will always be disabled, even if period is set.')
  );

  $form['info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Informations'),
  );

  $text = variable_get('commerce_closure_message', array('value' => '', 'format' => 'plain_text'));
  $form['info']['commerce_closure_message'] = array(
    '#type' => 'text_format',
    '#title' => t('Informative message to display'),
    '#description' => t('This message is displayed in a block.'),
    '#format' => $text['format'],
    '#default_value' => $text['value'],
  );


  return system_settings_form($form);
}